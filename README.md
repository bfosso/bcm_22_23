# ESERCITAZIONI PRATICHE DEL CORSO DI BIOINFORMATICA   

### Esercitazione 1 (22/12/2022)
#### Allineamento tra sequenze
- [X] [Allineamento](Allineamento_tra_sequenze/allineamento.md)

### Esercitazione 2 (17/01/2023)
#### Allineamento tra sequenze / Valutazione qualità dati di sequenziamento
- [X] [Utilizzo di BLAST e BLAT per l'analisi genomica](Allineamento_tra_sequenze/blast_genomico.md)
- [X] [Accesso al Server](Accesso_al_server.md)

### Esercitazione 3 (18/01/2023)
#### Analisi di dati di DNA-Metabarcoding
- [X] [Analisi di dati di DNA-Metabarcoding](Metabarcoding/metabarcoding.md)

Contatti:  
[bruno.fosso@uniba.it](mailto:bruno.fosso@uniba.it)